const UserModel = require('../models').user;
const JWT = require('jsonwebtoken');
require('dotenv').config();
const bcrypt = require('bcrypt');

class LoginController {
  async login(req, res) {
    try {
      const email = req.body.email;
      const password = req.body.password;

      const user = await UserModel.findOne({
        where: {
          email: email
        }
      })

      if (user === null) {
        res.status(400).send({
          message: "your email is wrong",
        })
      }

      const passwordUser = user.password;
      const comparePassword = bcrypt.compareSync(password, passwordUser);
      if (!comparePassword) {
        res.status(401).send({
          message: "you failed login"
        })
      }
      
      const token = JWT.sign(
        { user : user.email, username : user.username  },
        //'SECRETKEY123'
        process.env.SECRETKEY
      );

      res.status(200).send({
        message: "your email is right",
        token
      });

    } catch (error) {
      console.log('error', error)
      res.status(500).send({
        message: "email salah"
      })
    }
  }

}
module.exports = LoginController;