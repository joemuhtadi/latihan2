const User = require('../models').user;
const TrainingMaster = require('../models').trainingmaster;
const bcrypt = require('bcrypt');

class UserController {
  async list(req, res){
    try {
      const user = await User.findAll({
        include: ['datadiri', {
          model: TrainingMaster,
          as: 'datatrainings',
          attributes: ["id","nama_training","tanggal_training"],
          through: {
            attributes: [],
          }
        }]
      });

      console.log(user.length)

      if(user.length === 0) {
        res.status(400).send({
          error : "data empty"
        });
      } 

      res.status(200).send({
        message: `Selamat datang mas ${req.user.username}`,
        data: user
      });
      

    } catch (error){
      res.status(500).send(error);
    }
  }

  async create(req, res) {

    console.log(req.body)
    try {
      const password = req.body.password;

      await User.create({
        username  : req.body.username,
        email     : req.body.email,
        password  : password
      })
      res.status(200).send({
        message: 'yeaaaa, you sucess insert data!!!'
      })
    } catch (error) {
      res.status(500).send({
        message: 'you wrong!!!'
      })
    }
    
  }
}

module.exports = UserController;
