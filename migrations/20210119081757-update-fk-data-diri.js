'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
     await queryInterface.changeColumn(
       'datadiris', 
       'idUser',
       {
        type : Sequelize.INTEGER,
        references: {
          model: 'users',
          key: 'id',
        }
       }
      );
    
  },

  down: async (queryInterface, Sequelize) => {
      await queryInterface.changeColumn(
        'datadiris', 
        'idUser',
        {
          type : Sequelize.INTEGER
        }
        );
  }
};
