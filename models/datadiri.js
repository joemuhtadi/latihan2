'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class datadiri extends Model {

    static associate(models) {
      
      this.belongsTo(models.user, {
        foreignKey: 'idUser',
        as: 'datauser'
      });
  
    }
  };
  datadiri.init({
    nama: DataTypes.STRING,
    jabatan: DataTypes.STRING,
    idUser: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'datadiri',
  });
  return datadiri;
};