'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class historytraining extends Model {

    
    static associate(models) {
      /*
      this.hasMany(models.user, {
        foreignKey: 'idUser',
        as: 'datauser'
      })

      this.hasMany(models.trainingmaster, {
        foreignKey: 'idTraining',
        as: 'datatraining'
      })
      */
    }
    
  };
  historytraining.init({
    idUser: DataTypes.INTEGER,
    idTraining: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'historytraining',
  });
  return historytraining;
};