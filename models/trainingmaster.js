'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class trainingmaster extends Model {

    static associate(models) {
      
      this.belongsToMany(models.user, {
        through: 'historytrainings',
        foreignKey: 'idTraining',
        as: 'users'
      })
      
    }
  };
  trainingmaster.init({
    nama_training: DataTypes.STRING,
    tanggal_training: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'trainingmaster',
  });
  return trainingmaster;
};