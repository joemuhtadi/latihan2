'use strict';
const bcrypt = require('bcrypt');

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {

    static associate(models) {

      this.hasOne(models.datadiri, {
        foreignKey: 'idUser',
        as: 'datadiri'
      })

      this.belongsToMany(models.trainingmaster, {
        through: 'historytrainings',
        foreignKey: 'idUser',
        as: 'datatrainings'
      })
    
    }
  };
  user.init({
    username: DataTypes.STRING,
    password: {
      type : DataTypes.STRING,
      set(password) {
        const saltRounds = 10;

        const salt = bcrypt.genSaltSync(saltRounds);
        const passwordHashed = bcrypt.hashSync(password, salt);
        this.setDataValue('password', passwordHashed);
      }
    },
    email: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user',
  });
  return user;
};