var express = require('express');
var router = express.Router();


const UserController = require('../controllers/user')
const LoginController = require('../controllers/login');
const passport = require('passport');

const loginController = new LoginController();
const userController = new UserController();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get(
  '/api/user', 
  passport.authenticate('jwt', { 'session': false }),
  userController.list
);

router.post('/api/login', loginController.login);

router.post('/api/user', userController.create);

module.exports = router;

