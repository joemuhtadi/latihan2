'use strict';

module.exports = {

  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('datadiris', [{
      nama: 'Binar Nusantara',
      jabatan: 'Programmer Binar',
      idUser: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }]);
  
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('datadiris', null, {});
  }
};
