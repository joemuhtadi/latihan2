'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

      return queryInterface.bulkInsert('trainingmasters', [{
        nama_training: 'Binar Nusantara',
        tanggal_training: '2020-10-01',
        createdAt: new Date(),
        updatedAt: new Date(),
      }]);
  
    },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('trainingmasters', null, {});
  }
};
