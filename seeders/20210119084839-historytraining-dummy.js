'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('historytrainings', [
      {
        idUser: '1',
        idTraining: '1',
        createdAt: new Date(),
        updatedAt: new Date(),
      }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('historytrainings', null, {});
  }
};
