// Import the dependencies for testing
const chai = require('chai')
const chaiHttp = require('chai-http')
const sinon = require('sinon');
const app = require('../../app')
const User = require('../../models').user;


const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoianVuQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoianVuIiwiaWF0IjoxNjEyNTEwOTI1fQ.RY56F2HkBWGx5X7OOVgLAukoPUmqhly5FgkhJQkYteM';

// Configure chai
chai.use(chaiHttp);
chai.should();

describe("User", () => {
    describe("GET /api/user", () => {
      beforeEach(function () {
        sinon.restore();
      });
  
      it("harus mereturn hasil berupa daftar user", (done) => {
        
        chai.request(app)
          .get('/api/user')
          .set({ "Authorization": `Bearer ${token}` })
          .end((err, res) => {
            console.log('ini hasilnya', res.body);
            res.should.have.status(200);
            res.body.should.to.be.an('object');
            res.body.should.to.have.property('message');
            done();
          });
      });
      
      it("harus berhasil login", (done) => {
        
        chai.request(app)
          .post('/api/login')
          .set('content-type', 'application/json')
          .send({email: 'jun@gmail.com', password: '1234'})
          .end((err, res) => {
            console.log('ini hasilnya', res.body);
            res.should.have.status(200);
            res.body.should.to.be.an('object');
            res.body.should.to.have.property('message');
            res.body.should.to.have.property('token');
            done();
          });
      });

      it("harus berhasil register", (done) => {
        
        chai.request(app)
          .post('/api/user')
          .set('content-type', 'application/json')
          .send({email: 'fajri@gmail.com', password: '1234', username:'fajri'})
          .end((err, res) => {
            console.log('ini hasilnya', res.body);
            res.should.have.status(200);
            res.body.should.to.be.an('object');
            res.body.should.to.have.property('message');
            done();
          });
      });

      it("jika database system error", (done) => {
        
        sinon.stub(User, "findAll").throws(new Error('error database'));

        chai.request(app)
          .get('/api/user')
          .set({ "Authorization": `Bearer ${token}` })
          .end((err, res) => {
            console.log('ini hasilnya', res.body);
            res.should.have.status(500);
            done();
          });
      });

      it("harus mereturn error 400 karena user tidak ada", (done) => {
        sinon.stub(User, "findAll").callsFake(() => {
          return []
        })
  
        chai.request(app)
          .get('/api/user')
          .set({ "Authorization": `Bearer ${token}` })
          .end((err, res) => {
            console.log('ini hasilnya', res.body);
            res.should.have.status(400);
            done();
          });
      });

      /*
      it("harus mereturn error 400 karena user tidak ada", (done) => {
        sinon.stub(User, "findAll").callsFake(() => {
          return []
        })
  
        chai.request(app)
          .get('/api/user')
          .set({ "Authorization": `Bearer ${token}` })
          .end((err, res) => {
            console.log('ini hasilnya', res.body);
            res.should.have.status(400);
            done();
          });
      });
      */

      /*
      it("harus mereturn error 500", (done) => {
        sinon.stub(User, "findAll").throws(new Error('error database'));
  
  
        chai.request(app)
          .get('/api/user')
          .set({ "Authorization": `Bearer ${token}` })
          .end((err, res) => {
            console.log('ini hasilnya', res.body);
            res.should.have.status(500);
            done();
          });
      });
      */

    });
    
  });

